package ru.agk13145.education.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserTypeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkRedirect() throws Exception {
        this.mockMvc
                .perform(
                        get("/topic")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void checkAuthorization() throws Exception {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJiMDhmODZhZi0zNWRhLTQ4ZjItOGZhYi1jZWYzOTA0NjYwYmQifQ.-xN_h82PHVTCMA9vdoHrcZxH-x5mb11y1537t3rGzcM";
        this.mockMvc
                .perform(
                        get("/topic")
                                .contentType(MediaType.APPLICATION_JSON)
                                .cookie(new Cookie("token", token))
                                .header("Set-Cookie", "token=" + token + "; domain=localhost; path=/" )
                )
                .andExpect(status().is3xxRedirection());
    }

}
