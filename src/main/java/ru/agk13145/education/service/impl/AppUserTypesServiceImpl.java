package ru.agk13145.education.service.impl;

import org.springframework.stereotype.Service;
import ru.agk13145.education.dao.AppUserTypeDao;
import ru.agk13145.education.model.AppUserType;
import ru.agk13145.education.service.AppUserTypeService;

import java.util.List;

@Service
public class AppUserTypesServiceImpl implements AppUserTypeService {

    private final AppUserTypeDao appUserTypeDao;

    public AppUserTypesServiceImpl(AppUserTypeDao appUserTypeDao) {
        this.appUserTypeDao = appUserTypeDao;
    }

    @Override
    public AppUserType getAppUserType(Long appUserTypeId) {
        return appUserTypeDao.getAppUserType(appUserTypeId);
    }

    @Override
    public Long createAppUserType(AppUserType appUserType) {
        return appUserTypeDao.createAppUserType(appUserType);
    }

    @Override
    public void updateAppUserType(AppUserType appUserType) {
        appUserTypeDao.updateAppUserType(appUserType);
    }

    @Override
    public void deleteAppUserType(Long appUserTypeId) {
        appUserTypeDao.deleteAppUserType(appUserTypeId);
    }

    @Override
    public List<AppUserType> getAppUserTypes() {
        return appUserTypeDao.getAppUserTypes();
    }

}
