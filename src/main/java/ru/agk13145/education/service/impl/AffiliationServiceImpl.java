package ru.agk13145.education.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.agk13145.education.dao.AffiliationDao;
import ru.agk13145.education.model.Affiliation;
import ru.agk13145.education.service.AffiliationService;

import java.util.List;

@Service
public class AffiliationServiceImpl implements AffiliationService {

    @Autowired
    AffiliationDao affiliationDao;

    @Override
    public Affiliation getAffiliation(Long affiliationId) {
        return affiliationDao.getAffiliation(affiliationId);
    }

    @Override
    public Long createAffiliation(Affiliation affiliation) {
        return affiliationDao.createAffiliation(affiliation);
    }

    @Override
    public void updateAffiliation(Affiliation affiliation) {
        affiliationDao.updateAffiliation(affiliation);
    }

    @Override
    public void deleteAffiliation(Long affiliationId) {
        affiliationDao.deleteAffiliation(affiliationId);
    }

    @Override
    public List<Affiliation> getAffiliations() {
        return affiliationDao.getAffiliations();
    }
}
