package ru.agk13145.education.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.agk13145.education.dao.ApplicationStatusTypeDao;
import ru.agk13145.education.model.ApplicationStatusType;
import ru.agk13145.education.service.ApplicationStatusTypeService;

import java.util.List;

@Service
public class ApplicationStatusTypeServiceImpl implements ApplicationStatusTypeService {

    @Autowired
    ApplicationStatusTypeDao applicationStatusTypeDao;

    @Override
    public ApplicationStatusType getApplicationStatusType(Long applicationStatusTypeId) {
        return applicationStatusTypeDao.getApplicationStatusType(applicationStatusTypeId);
    }

    @Override
    public Long createApplicationStatusType(ApplicationStatusType applicationStatusType) {
        return applicationStatusTypeDao.createApplicationStatusType(applicationStatusType);
    }

    @Override
    public void updateApplicationStatusType(ApplicationStatusType applicationStatusType) {
        applicationStatusTypeDao.updateApplicationStatusType(applicationStatusType);
    }

    @Override
    public void deleteApplicationStatusType(Long applicationStatusTypeId) {
        applicationStatusTypeDao.deleteApplicationStatusType(applicationStatusTypeId);
    }

    @Override
    public List<ApplicationStatusType> getApplicationStatusTypes() {
        return applicationStatusTypeDao.getApplicationStatusTypes();
    }
}
