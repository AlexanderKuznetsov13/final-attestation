package ru.agk13145.education.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.agk13145.education.dao.TopicDao;
import ru.agk13145.education.model.Topic;
import ru.agk13145.education.service.TopicService;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {

    @Autowired
    TopicDao topicDao;

    @Override
    public Topic getTopic(Long topicId) {
        return topicDao.getTopic(topicId);
    }

    @Override
    @Transactional
    public Long createTopic(Topic topic) {
        return topicDao.createTopic(topic);
    }

    @Override
    @Transactional
    public void updateTopic(Topic topic) {
        topicDao.updateTopic(topic);
    }

    @Override
    @Transactional
    public void deleteTopic(Long topicId) {
        topicDao.deleteTopic(topicId);
    }

    @Override
    public List<Topic> getTopics() {
        return topicDao.getTopics();
    }
}
