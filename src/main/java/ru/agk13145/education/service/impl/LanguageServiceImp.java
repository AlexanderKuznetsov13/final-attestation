package ru.agk13145.education.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.agk13145.education.dao.LanguageDao;
import ru.agk13145.education.model.Language;
import ru.agk13145.education.service.LanguageService;

import java.util.List;

@Service
public class LanguageServiceImp implements LanguageService {


    @Autowired
    LanguageDao languageDao;
    @Override
    public Language getLanguage(Integer languageId) {
        return languageDao.getLanguage(languageId);
    }

    @Override
    public Integer createLanguage(Language language) {
        return languageDao.createLanguage(language);
    }

    @Override
    public void updateLanguage(Language language) {
        languageDao.updateLanguage(language);
    }

    @Override
    public void deleteLanguage(Integer languageId) {
        languageDao.deleteLanguage(languageId);
    }

    @Override
    public List<Language> getLanguages() {
        return languageDao.getLanguages();
    }
}
