package ru.agk13145.education.service;


import ru.agk13145.education.model.User;

public interface UserService {

    User getUser(Integer userId);

    User getUserByLogin(String login);

    User getUserByLoginAndPassword(String login, String password);
}
