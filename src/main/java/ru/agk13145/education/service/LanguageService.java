package ru.agk13145.education.service;

import ru.agk13145.education.model.Language;

import java.util.List;

public interface LanguageService {

    Language getLanguage(Integer languageId);

    Integer createLanguage(Language language);

    void updateLanguage(Language language);

    void deleteLanguage(Integer languageId);

    List<Language> getLanguages();

}
