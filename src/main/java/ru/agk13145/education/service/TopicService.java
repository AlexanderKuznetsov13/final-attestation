package ru.agk13145.education.service;


import ru.agk13145.education.model.Topic;

import java.util.List;

public interface TopicService {
    Topic getTopic(Long topicId);

    Long createTopic(Topic topic);

    void updateTopic(Topic topic);

    void deleteTopic(Long topicId);

    List<Topic> getTopics();
}
