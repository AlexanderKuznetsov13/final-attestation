package ru.agk13145.education.service;

import ru.agk13145.education.model.ApplicationStatusType;

import java.util.List;

public interface ApplicationStatusTypeService {
    ApplicationStatusType getApplicationStatusType(Long applicationStatusTypeId);

    Long createApplicationStatusType(ApplicationStatusType applicationStatusType);

    void updateApplicationStatusType(ApplicationStatusType applicationStatusType);

    void deleteApplicationStatusType(Long applicationStatusTypeId);

    List<ApplicationStatusType> getApplicationStatusTypes();
}
