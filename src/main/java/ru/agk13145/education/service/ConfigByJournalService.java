package ru.agk13145.education.service;


import ru.agk13145.education.model.ConfigByJournal;

import java.util.List;

public interface ConfigByJournalService {
    ConfigByJournal getConfigByJournal(Integer configByJournalId);

    Integer createConfigByJournal(ConfigByJournal configByJournal);

    void updateConfigByJournal(ConfigByJournal configByJournal);

    void deleteConfigByJournal(Integer configByJournalId);

    List<ConfigByJournal> getConfigByJournals();
}
