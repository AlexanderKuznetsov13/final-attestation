package ru.agk13145.education.service;


import ru.agk13145.education.model.AuthorByApplication;

import java.util.List;

public interface AuthorByApplicationService {
    AuthorByApplication getAuthorByApplication(Integer authorByApplicationId);

    Integer createAuthorByApplication(AuthorByApplication authorByApplication);

    void updateAuthorByApplication(AuthorByApplication authorByApplication);

    void deleteAuthorByApplication(Integer authorByApplicationId);

    List<AuthorByApplication> getAuthorByApplications();
}
