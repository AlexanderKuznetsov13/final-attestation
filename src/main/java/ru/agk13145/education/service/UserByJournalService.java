package ru.agk13145.education.service;


import ru.agk13145.education.model.UserByJournal;

import java.util.List;

public interface UserByJournalService {

    UserByJournal getUserByJournal(Integer userByJournalId);

    Integer createUserByJournal(UserByJournal userByJournal);

    void updateUserByJournal(UserByJournal userByJournal);

    void deleteUserByJournal(Integer userByJournalId);

    List<UserByJournal> getUserByJournals();
}
