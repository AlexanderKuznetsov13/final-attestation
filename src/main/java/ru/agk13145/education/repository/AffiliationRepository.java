package ru.agk13145.education.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.agk13145.education.entity.AffiliationEntity;

public interface AffiliationRepository extends JpaRepository<AffiliationEntity, Long> {
}
