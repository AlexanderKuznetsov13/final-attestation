package ru.agk13145.education.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.agk13145.education.entity.TopicEntity;

public interface TopicRepository extends JpaRepository<TopicEntity, Long> {
}
