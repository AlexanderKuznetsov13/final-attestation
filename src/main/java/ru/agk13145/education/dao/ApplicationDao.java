package ru.agk13145.education.dao;


import ru.agk13145.education.model.Application;

import java.util.List;

public interface ApplicationDao {
    Application getApplication(Integer applicationId);

    Integer createApplication(Application application);

    void updateApplication(Application application);

    void deleteApplication(Integer applicationId);

    List<Application> getApplications();

}
