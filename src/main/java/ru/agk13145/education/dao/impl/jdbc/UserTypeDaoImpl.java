package ru.agk13145.education.dao.impl.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.agk13145.education.dao.UserTypeDao;
import ru.agk13145.education.model.UserType;
import ru.agk13145.education.model.mapper.rowmapper.UserTypeRowMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserTypeDaoImpl implements UserTypeDao {

    private final static String SQL_SELECT_USERTYPE = "SELECT ID, NAME, DESCRIPTION FROM sciencejournal.usertype " +
            "WHERE id = :id";

    private final static String SQL_CREATE_USERTYPE = "INSERT INTO " +
            "sciencejournal.usertype(name, description) VALUES (:name,:description )";

    private final static String SQL_UPDATE_USERTYPE = "UPDATE sciencejournal.usertype " +
            "SET name = :name, description=:description WHERE id = :id";

    private final static String SQL_DELETE_USERTYPE = "DELETE FROM sciencejournal.usertype WHERE id = :id";

    private final static String SQL_SELECT_ALL_USERTYPE = "SELECT ID, NAME, DESCRIPTION  FROM sciencejournal.usertype ORDER BY id ASC";


    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public UserType getUserType(Integer usertypeId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", usertypeId);
        return namedParameterJdbcTemplate.queryForObject(SQL_SELECT_USERTYPE, paramMap, new UserTypeRowMapper());
    }

    @Override
    @Transactional
    public Integer createUserType(UserType usertype) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("name", usertype.getName());
        namedParameters.addValue("description", usertype.getDescription());
        namedParameterJdbcTemplate.update(SQL_CREATE_USERTYPE, namedParameters, generatedKeyHolder);
        return (Integer) generatedKeyHolder.getKeys().get("id");
    }

    @Override
    @Transactional
    public void updateUserType(UserType usertype) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", usertype.getId());
        params.put("name", usertype.getName());
        params.put("description", usertype.getDescription());
        namedParameterJdbcTemplate.update(SQL_UPDATE_USERTYPE, params);
    }

    @Override
    @Transactional
    public void deleteUserType(Integer usertypeId) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", usertypeId);
        namedParameterJdbcTemplate.update(SQL_DELETE_USERTYPE, params);
    }

    @Override
    public List<UserType> getUserTypes() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_USERTYPE, new UserTypeRowMapper());
    }
}
