package ru.agk13145.education.dao.impl.springdata;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.agk13145.education.dao.AffiliationDao;
import ru.agk13145.education.entity.AffiliationEntity;
import ru.agk13145.education.model.Affiliation;
import ru.agk13145.education.model.mapper.mapstruct.AffiliationMapper;
import ru.agk13145.education.repository.AffiliationRepository;

import java.util.List;
import java.util.stream.Collectors;

@Primary
@Repository
public class AffiliationDaoSpringDataImpl implements AffiliationDao {

    private final AffiliationRepository affiliationRepository;
    private final AffiliationMapper affiliationMapper;

    public AffiliationDaoSpringDataImpl(AffiliationRepository affiliationRepository, AffiliationMapper affiliationMapper) {
        this.affiliationRepository = affiliationRepository;
        this.affiliationMapper = affiliationMapper;
    }


    @Override
    public Affiliation getAffiliation(Long affiliationId) {
        return affiliationRepository.findById(affiliationId).map(affiliationMapper::toModel).orElse(null);
    }

    @Override
    @Transactional
    public Long createAffiliation(Affiliation affiliation) {
        AffiliationEntity entity = affiliationMapper.toEntity(affiliation);
        return affiliationRepository.save(entity).getId();
    }

    @Override
    @Transactional
    public void updateAffiliation(Affiliation affiliation) {
        AffiliationEntity entity = affiliationMapper.toEntity(affiliation);
        affiliationRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteAffiliation(Long affiliationId) {
        affiliationRepository.deleteById(affiliationId);
    }

    @Override
    public List<Affiliation> getAffiliations() {
        List<Affiliation> affiliationList = affiliationRepository.findAll()
                .stream()
                .map(affiliationMapper::toModel)
                .collect(Collectors.toList());
        return affiliationList;
    }
}
