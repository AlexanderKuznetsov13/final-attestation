package ru.agk13145.education.dao.impl.springdata;

import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.agk13145.education.dao.TopicDao;
import ru.agk13145.education.entity.TopicEntity;
import ru.agk13145.education.model.Topic;
import ru.agk13145.education.model.mapper.mapstruct.TopicMapper;
import ru.agk13145.education.model.mapper.rowmapper.TopicRowMapper;
import ru.agk13145.education.repository.TopicRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Primary
@Repository
public class TopicDaoSpringDataImpl implements TopicDao {

    private final TopicRepository topicRepository;
    private final TopicMapper topicMapper;

    public TopicDaoSpringDataImpl(TopicRepository topicRepository, TopicMapper topicMapper) {
        this.topicRepository = topicRepository;
        this.topicMapper = topicMapper;
    }


    @Override
    public Topic getTopic(Long topicId) {
        return topicRepository.findById(topicId).map(topicMapper::toModel).orElse(null);
    }

    @Override
    @Transactional
    public Long createTopic(Topic topic) {
        TopicEntity entity = topicMapper.toEntity(topic);
        return topicRepository.save(entity).getId();
    }

    @Override
    @Transactional
    public void updateTopic(Topic topic) {
        TopicEntity entity = topicMapper.toEntity(topic);
        topicRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteTopic(Long topicId) {
        topicRepository.deleteById(topicId);
    }

    @Override
    public List<Topic> getTopics() {
        List<Topic> topicList = topicRepository.findAll()
                .stream()
                .map(topicMapper::toModel)
                .collect(Collectors.toList());
        return topicList;
    }
}
