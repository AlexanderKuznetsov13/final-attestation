package ru.agk13145.education.dao.impl.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.agk13145.education.dao.AppUserTypeDao;
import ru.agk13145.education.dao.AppUserTypeDao;
import ru.agk13145.education.model.AppUserType;
import ru.agk13145.education.model.mapper.rowmapper.AppUserTypeRowMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AppUserTypeDaoImpl implements AppUserTypeDao {

    private final static String SQL_SELECT_APPUSERTYPE = "SELECT ID, NAME, DESCRIPTION FROM sciencejournal.appusertype " +
            "WHERE id = :id";

    private final static String SQL_CREATE_APPUSERTYPE = "INSERT INTO " +
            "sciencejournal.appusertype(name, description) VALUES (:name, :description )";

    private final static String SQL_UPDATE_APPUSERTYPE = "UPDATE sciencejournal.appusertype " +
            "SET name = :name, description=:description WHERE id = :id";

    private final static String SQL_DELETE_APPUSERTYPE = "DELETE FROM sciencejournal.appusertype WHERE id = :id";

    private final static String SQL_SELECT_ALL_APPUSERTYPE = "SELECT ID, NAME, DESCRIPTION  FROM sciencejournal.appusertype ORDER BY id ASC";


    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public AppUserType getAppUserType(Long appUsertypeId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", appUsertypeId);
        return namedParameterJdbcTemplate.queryForObject(SQL_SELECT_APPUSERTYPE, paramMap, new AppUserTypeRowMapper());
    }

    @Override
    @Transactional
    public Long createAppUserType(AppUserType appUserType) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("name", appUserType.getName());
        namedParameters.addValue("description", appUserType.getDescription());
        namedParameterJdbcTemplate.update(SQL_CREATE_APPUSERTYPE, namedParameters, generatedKeyHolder);
        return (Long) generatedKeyHolder.getKeys().get("id");
    }

    @Override
    @Transactional
    public void updateAppUserType(AppUserType appUserType) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", appUserType.getId());
        params.put("name", appUserType.getName());
        params.put("description", appUserType.getDescription());
        namedParameterJdbcTemplate.update(SQL_UPDATE_APPUSERTYPE, params);
    }

    @Override
    @Transactional
    public void deleteAppUserType(Long appUsertypeId) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", appUsertypeId);
        namedParameterJdbcTemplate.update(SQL_DELETE_APPUSERTYPE, params);
    }

    @Override
    public List<AppUserType> getAppUserTypes() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_APPUSERTYPE, new AppUserTypeRowMapper());
    }
}
