package ru.agk13145.education.dao.impl.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.agk13145.education.dao.UserDao;
import ru.agk13145.education.model.User;
import ru.agk13145.education.model.mapper.rowmapper.UserRowMapper;

import java.util.HashMap;
import java.util.Map;


@Repository
public class UserDaoImpl implements UserDao {

    private final static String SQL_SELECT_USER = "SELECT ID, NAME, SURNAME, MIDDLE_NAME, EMAIL, LOGIN, PASSWORD, APPUSERTYPE_ID FROM sciencejournal.user " +
            "WHERE id = :id";

    private final static String SQL_SELECT_USER_BY_LOGIN = "SELECT ID, NAME, SURNAME, MIDDLE_NAME, EMAIL, LOGIN, PASSWORD, APPUSERTYPE_ID FROM sciencejournal.user " +
            "WHERE login = :login";

    private final static String SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD = "SELECT ID, NAME, SURNAME, MIDDLE_NAME, EMAIL, LOGIN, PASSWORD, APPUSERTYPE_ID FROM sciencejournal.user " +
            "WHERE login = :login and password = :password";

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public User getUser(Integer userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", userId);
        return namedParameterJdbcTemplate.queryForObject(SQL_SELECT_USER, paramMap, new UserRowMapper());
    }

    @Override
    public User getUserByLogin(String login) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("login", login);
        return namedParameterJdbcTemplate.queryForObject(SQL_SELECT_USER_BY_LOGIN, paramMap, new UserRowMapper());
    }

    @Override
    public User getUserByLoginAndPassword(String login, String password) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("login", login);
        paramMap.put("password", password);
        return namedParameterJdbcTemplate.queryForObject(SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD, paramMap, new UserRowMapper());
    }
}
