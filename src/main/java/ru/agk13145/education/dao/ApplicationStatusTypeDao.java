package ru.agk13145.education.dao;


import ru.agk13145.education.model.ApplicationStatusType;

import java.util.List;

public interface ApplicationStatusTypeDao {
    ApplicationStatusType getApplicationStatusType(Long applicationStatusTypeId);

    Long createApplicationStatusType(ApplicationStatusType applicationStatusType);

    void updateApplicationStatusType(ApplicationStatusType applicationStatusType);

    void deleteApplicationStatusType(Long applicationStatusTypeId);

    List<ApplicationStatusType> getApplicationStatusTypes();
}
