package ru.agk13145.education.dao;


import ru.agk13145.education.model.Affiliation;

import java.util.List;

public interface AffiliationDao {
    Affiliation getAffiliation(Long affiliationId);

    Long createAffiliation(Affiliation affiliation);

    void updateAffiliation(Affiliation affiliation);

    void deleteAffiliation(Long affiliationId);

    List<Affiliation> getAffiliations();

}
