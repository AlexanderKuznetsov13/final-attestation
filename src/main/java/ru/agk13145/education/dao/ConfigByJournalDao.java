package ru.agk13145.education.dao;


import ru.agk13145.education.model.ConfigByJournal;

import java.util.List;

public interface ConfigByJournalDao {
    ConfigByJournal getConfigByJournal(Integer configByJournalId);

    Integer createConfigByJournal(ConfigByJournal configByJournal);

    void updateConfigByJournal(ConfigByJournal configByJournal);

    void deleteConfigByJournal(Integer configByJournalId);

    List<ConfigByJournal> getConfigByJournals();

}
