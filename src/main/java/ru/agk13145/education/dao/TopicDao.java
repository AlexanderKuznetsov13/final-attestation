package ru.agk13145.education.dao;


import ru.agk13145.education.model.Topic;

import java.util.List;

public interface TopicDao {
    /**
     *
     * @param topicId
     * @return
     */
    Topic getTopic(Long topicId);

    Long createTopic(Topic topic);

    void updateTopic(Topic topic);

    void deleteTopic(Long topicId);

    List<Topic> getTopics();

}
