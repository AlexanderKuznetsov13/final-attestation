package ru.agk13145.education.dao;


import ru.agk13145.education.model.Journal;

import java.util.List;

public interface JournalDao {
    Journal getJournal(Integer journalId);

    Integer createJournal(Journal journal);

    void updateJournal(Journal journal);

    void deleteJournal(Integer journalId);

    List<Journal> getJournals();

}
