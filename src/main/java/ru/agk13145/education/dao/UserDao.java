package ru.agk13145.education.dao;


import ru.agk13145.education.model.User;

public interface UserDao {

    /**
     * Получение пользователя по идентификатору
     *
     * @return пользователя
     */
    User getUser(Integer userId);

    User getUserByLogin(String login);

    User getUserByLoginAndPassword(String login, String password);


}
