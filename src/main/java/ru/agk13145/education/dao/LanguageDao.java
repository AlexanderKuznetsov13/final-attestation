package ru.agk13145.education.dao;

import ru.agk13145.education.model.Language;

import java.util.List;

public interface LanguageDao {

    Language getLanguage(Integer languageId);

    Integer createLanguage(Language Language);

    void updateLanguage(Language Language);

    void deleteLanguage(Integer languageId);

    List<Language> getLanguages();


}
