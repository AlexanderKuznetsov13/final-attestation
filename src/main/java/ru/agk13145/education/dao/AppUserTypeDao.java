package ru.agk13145.education.dao;


import ru.agk13145.education.model.AppUserType;
import ru.agk13145.education.model.UserType;

import java.util.List;

public interface AppUserTypeDao {
    AppUserType getAppUserType(Long appUserTypeId);

    Long createAppUserType(AppUserType appUserType);

    void updateAppUserType(AppUserType appUserType);

    void deleteAppUserType(Long appUserTypeId);

    List<AppUserType> getAppUserTypes();

}
