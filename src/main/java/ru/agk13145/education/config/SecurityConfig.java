package ru.agk13145.education.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ru.agk13145.education.config.jwt.JWTAuthorizationFilter;
import ru.agk13145.education.config.jwt.JwtProvider;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String AUTH_URL = "/auth/login";
//    private final JwtFilter jwtFilter;
    private final JwtProvider jwtProvider;


    private final UserDetailsService userDetailsService;

    public SecurityConfig(JwtProvider jwtProvider, UserDetailsService userDetailsService) {
        this.jwtProvider = jwtProvider;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)





                .and()
                .authorizeRequests(
                        requests -> {
                                requests
                                        .antMatchers("/", "/register", "auth", "/auth/**", "/auth/login").permitAll()
                                        .antMatchers("/swagger-ui/**", "/v3/api-docs/**").permitAll()
                                        .antMatchers("/admin**").hasRole("ADMIN")
                                        .antMatchers("/usertype").hasRole("ADMIN")
                                        .antMatchers("/language").permitAll()
                                        .antMatchers("/topic").hasAnyRole("ADMIN","USER")
                                        .anyRequest().authenticated();
                        }
                )
                .addFilter(new CustomUsernamePasswordAuthenticationFilter(jwtProvider, authenticationManager()))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtProvider, userDetailsService))



                .formLogin()
                .loginPage("/auth/login")           // Replace Security Default login page to our own Login.tfl page
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/sciencejournal")
                .failureUrl("/auth/login?error")
                .permitAll()
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/auth/login?logout")
                .deleteCookies("JSESSIONID", "token")
                .invalidateHttpSession(true);


    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


//    @Bean
//    @Override
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        return super.authenticationManagerBean();
//    }
}
