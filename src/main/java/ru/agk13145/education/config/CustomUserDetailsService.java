package ru.agk13145.education.config;

import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.agk13145.education.model.AppUserType;
import ru.agk13145.education.model.User;
import ru.agk13145.education.service.AppUserTypeService;
import ru.agk13145.education.service.UserService;
import ru.agk13145.education.service.impl.UserServiceImpl;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;
    private final AppUserTypeService appUserTypeService;

    public CustomUserDetailsService(@Lazy UserServiceImpl userService, @Lazy AppUserTypeService appUserTypeService) {
        this.userService = userService;
        this.appUserTypeService = appUserTypeService;
    }

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //todo
        User user = userService.getUserByLogin(username);
        AppUserType userType = appUserTypeService.getAppUserType(user.getAppUserTypeId());
        return CustomUserDetails.fromUserEntityToCustomUserDetails(user, userType.getName());
    }
}
