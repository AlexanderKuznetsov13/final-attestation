package ru.agk13145.education.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import ru.agk13145.education.config.jwt.JwtProvider;
import ru.agk13145.education.dto.request.AuthRequestDto;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final JwtProvider jwtProvider;
    private final AuthenticationManager authenticationManager;

    private boolean postOnly = true;

    public CustomUsernamePasswordAuthenticationFilter(JwtProvider jwtProvider, AuthenticationManager authenticationManager) {
        AntPathRequestMatcher requestMatcher = new AntPathRequestMatcher(SecurityConfig.AUTH_URL, "POST");
        this.setRequiresAuthenticationRequestMatcher(requestMatcher);
        this.jwtProvider = jwtProvider;
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (this.postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        } else {
            String username = this.obtainUsername(request);
            username = username != null ? username : "";
            username = username.trim();
            String password = this.obtainPassword(request);
            password = password != null ? password : "";
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
            this.setDetails(request, authRequest);
            return this.authenticationManager.authenticate(authRequest);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        CustomUserDetails user = (CustomUserDetails) authResult.getPrincipal();
        String token = jwtProvider.generateToken(user.getUsername());

        response.setHeader("Authorization", "Bearer " + token );
        response.setHeader("Set-Cookie", "token=" + token + "; domain=localhost; path=/" );
//        response.addCookie(new Cookie("Authorization", "Bearer " + token));
//        request.setHeader("Authorization", "Bearer " + token );
        super.successfulAuthentication(request, response, chain, authResult);
    }

//    @Override
//    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
//        SecurityContextHolder.clearContext();
//        this.logger.trace("Failed to process authentication request", failed);
//        this.logger.trace("Cleared SecurityContextHolder");
//        this.logger.trace("Handling authentication failure");
//        this.rememberMeServices.loginFail(request, response);
//        this.failureHandler.onAuthenticationFailure(request, response, failed);
//    }
}
