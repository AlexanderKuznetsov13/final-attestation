package ru.agk13145.education.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.agk13145.education.config.jwt.JwtProvider;
import ru.agk13145.education.service.UserService;

import java.util.Optional;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class MainController {

    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public ModelAndView getMainPage(@RequestParam Optional<String> error) {
        return new ModelAndView("main", "error", error);
    }
}