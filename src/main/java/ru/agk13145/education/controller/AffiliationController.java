package ru.agk13145.education.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.agk13145.education.dto.request.AffiliationRequestDto;
import ru.agk13145.education.dto.response.AffiliationResponseDto;
import ru.agk13145.education.dto.response.TopicResponseDto;
import ru.agk13145.education.model.Affiliation;
import ru.agk13145.education.model.mapper.mapstruct.AffiliationMapper;
import ru.agk13145.education.service.AffiliationService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/affiliation", produces = "application/json;charset=UTF-8")
public class AffiliationController {

    private final AffiliationService affiliationService;

    private final AffiliationMapper affiliationMapper;

    public AffiliationController(AffiliationService affiliationService, AffiliationMapper affiliationMapper) {
        this.affiliationService = affiliationService;
        this.affiliationMapper = affiliationMapper;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<AffiliationResponseDto>> getAffiliations() {
        List<AffiliationResponseDto> list = affiliationService.getAffiliations()
                .stream()
                .map(affiliationMapper::toResponseDto)
                .collect(Collectors.toList());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/{affiliationId}")
    @ResponseBody
    public ResponseEntity<AffiliationResponseDto> getAffiliation(@PathVariable Long affiliationId) {
        Affiliation affiliation = affiliationService.getAffiliation(affiliationId);
        return new ResponseEntity<>(affiliationMapper.toResponseDto(affiliation), HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<AffiliationResponseDto> createAffiliation(@RequestBody AffiliationRequestDto requestDto) {
        Affiliation affiliation = affiliationMapper.toModel(requestDto);
        Long affiliationId = affiliationService.createAffiliation(affiliation);
        affiliation = affiliationService.getAffiliation(affiliationId);
        return new ResponseEntity<>(affiliationMapper.toResponseDto(affiliation), HttpStatus.OK);
    }

    @PutMapping(value = "/{affiliationId}")
    @ResponseBody
    public ResponseEntity<AffiliationResponseDto> updateAffiliation(@PathVariable Long affiliationId, @RequestBody AffiliationRequestDto requestDto) {
        Affiliation affiliation = affiliationMapper.toModel(requestDto);
        affiliation.setId(affiliationId);
        affiliationService.updateAffiliation(affiliation);
        return new ResponseEntity<>(affiliationMapper.toResponseDto(affiliation), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{affiliationId}")
    public ResponseEntity<Long> deleteAffiliation(@PathVariable Long affiliationId) {
        affiliationService.deleteAffiliation(affiliationId);
        return new ResponseEntity<>(affiliationId, HttpStatus.OK);
    }

}
