package ru.agk13145.education.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.agk13145.education.dto.request.TopicRequestDto;
import ru.agk13145.education.dto.response.TopicResponseDto;
import ru.agk13145.education.model.Topic;
import ru.agk13145.education.model.mapper.mapstruct.TopicMapper;
import ru.agk13145.education.service.TopicService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/topic", produces = "application/json;charset=UTF-8")
public class TopicController {

    private final TopicService topicService;
    private final TopicMapper topicMapper;

    public TopicController(TopicService topicService, TopicMapper topicMapper) {
        this.topicService = topicService;
        this.topicMapper = topicMapper;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<TopicResponseDto>> getTopics() {
        List<TopicResponseDto> topics = topicService.getTopics()
                .stream()
                .map(topicMapper::toResponseDto)
                .collect(Collectors.toList());
        return new ResponseEntity<>(topics, HttpStatus.OK);
    }

    @GetMapping(value = "/{topicId}")
    @ResponseBody
    public ResponseEntity<TopicResponseDto> getTopic(@PathVariable Long topicId) {
        Topic topic = topicService.getTopic(topicId);
        return new ResponseEntity<>(topicMapper.toResponseDto(topic), HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<TopicResponseDto> createTopic(@RequestBody TopicRequestDto requestDto) {
        Topic topic = topicMapper.toModel(requestDto);
        Long topicId = topicService.createTopic(topic);
        topic = topicService.getTopic(topicId);
        return new ResponseEntity<>(topicMapper.toResponseDto(topic), HttpStatus.OK);
    }

    @PutMapping(value = "/{topicId}")
    @ResponseBody
    public ResponseEntity<TopicResponseDto> updateTopic(@PathVariable Integer topicId, @RequestBody TopicRequestDto requestDto) {
        Topic topic = topicMapper.toModel(requestDto);
        topic.setId(topicId);
        topicService.updateTopic(topic);
        return new ResponseEntity<>(topicMapper.toResponseDto(topic), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{topicId}")
    public ResponseEntity<Long> deleteTopic(@PathVariable Long topicId) {
        topicService.deleteTopic(topicId);
        return new ResponseEntity<>(topicId, HttpStatus.OK);
    }

}
