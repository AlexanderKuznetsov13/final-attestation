package ru.agk13145.education.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.agk13145.education.config.jwt.JwtProvider;
import ru.agk13145.education.dto.request.AuthRequestDto;
import ru.agk13145.education.dto.response.AuthResponseDto;
import ru.agk13145.education.model.User;
import ru.agk13145.education.service.UserService;

import javax.validation.Valid;
import java.util.Optional;

@Tag(name = "Авторизация")
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;
    private final JwtProvider jwtProvider;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView getLoginPage(@RequestParam Optional<String> error) {
        return new ModelAndView("login", "error", error);
    }

    @PostMapping(value = "/login", produces = "application/json;charset=UTF-8")
    public AuthResponseDto auth( AuthRequestDto request) {
        User user = userService.getUserByLoginAndPassword(request.getUsername(), request.getPassword());
        //todo NPE
        String token = jwtProvider.generateToken(user.getLogin());
        return new AuthResponseDto(token);
    }
}
