package ru.agk13145.education.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.agk13145.education.model.User;
import ru.agk13145.education.service.UserService;


@Controller
@RequestMapping(value = "/user", produces = "application/json;charset=UTF-8")
public class UserController {

    @Autowired
    private UserService userService;

//    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
//    public String index(Model model) {
//        return "index";
//    }

    @RequestMapping(value = "/author/{agentId}" , method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<User> getAuthor(@PathVariable String agentId) {
        User agent = userService.getUser(Integer.parseInt(agentId));
        return new ResponseEntity<>(agent, HttpStatus.OK);
    }

}