package ru.agk13145.education.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.agk13145.education.dto.request.ApplicationStatusTypeRequestDto;
import ru.agk13145.education.dto.response.ApplicationStatusTypeResponseDto;
import ru.agk13145.education.model.Affiliation;
import ru.agk13145.education.model.ApplicationStatusType;
import ru.agk13145.education.model.mapper.mapstruct.ApplicationStatusTypeMapper;
import ru.agk13145.education.service.ApplicationStatusTypeService;


import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/applicationstatustype", produces = "application/json;charset=UTF-8")
public class ApplicationStatusTypeController {

    private final ApplicationStatusTypeService applicationStatusTypeService;

    private final ApplicationStatusTypeMapper applicationStatusTypeMapper;

    public ApplicationStatusTypeController(ApplicationStatusTypeService applicationStatusTypeService, ApplicationStatusTypeMapper applicationStatusTypeMapper) {
        this.applicationStatusTypeService = applicationStatusTypeService;
        this.applicationStatusTypeMapper = applicationStatusTypeMapper;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<ApplicationStatusTypeResponseDto>> getApplicationStatusTypes() {
        List<ApplicationStatusTypeResponseDto> list = applicationStatusTypeService.getApplicationStatusTypes()
                .stream()
                .map(applicationStatusTypeMapper::toResponseDto)
                .collect(Collectors.toList());;
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/{applicationStatusTypeId}")
    @ResponseBody
    public ResponseEntity<ApplicationStatusTypeResponseDto> getApplicationStatusType(@PathVariable Long applicationStatusTypeId) {
        ApplicationStatusType applicationStatusType = applicationStatusTypeService.getApplicationStatusType(applicationStatusTypeId);
        return new ResponseEntity<>(applicationStatusTypeMapper.toResponseDto(applicationStatusType), HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<ApplicationStatusTypeResponseDto> createApplicationStatusType(@RequestBody ApplicationStatusTypeRequestDto requestDto) {
        ApplicationStatusType applicationStatusType = applicationStatusTypeMapper.toModel(requestDto);
        Long applicationStatusTypeId = applicationStatusTypeService.createApplicationStatusType(applicationStatusType);
        applicationStatusType = applicationStatusTypeService.getApplicationStatusType(applicationStatusTypeId);
        return new ResponseEntity<>(applicationStatusTypeMapper.toResponseDto(applicationStatusType), HttpStatus.OK);
    }

    @PutMapping(value = "/{applicationStatusTypeId}")
    @ResponseBody
    public ResponseEntity<ApplicationStatusTypeResponseDto> updateApplicationStatusType(@PathVariable Long applicationStatusTypeId, @RequestBody ApplicationStatusTypeRequestDto requestDto) {
        ApplicationStatusType applicationStatusType = applicationStatusTypeMapper.toModel(requestDto);
        applicationStatusType.setId(applicationStatusTypeId);
        applicationStatusTypeService.updateApplicationStatusType(applicationStatusType);
        return new ResponseEntity<>(applicationStatusTypeMapper.toResponseDto(applicationStatusType), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{applicationStatusTypeId}")
    public ResponseEntity<Long> deleteApplicationStatusType(@PathVariable Long applicationStatusTypeId) {
        applicationStatusTypeService.deleteApplicationStatusType(applicationStatusTypeId);
        return new ResponseEntity<>(applicationStatusTypeId, HttpStatus.OK);
    }

}
