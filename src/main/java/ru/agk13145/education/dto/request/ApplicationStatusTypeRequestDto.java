package ru.agk13145.education.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "Статус заявки")
public class ApplicationStatusTypeRequestDto {
    private Long id;
    private String name;
    private String description;
}
