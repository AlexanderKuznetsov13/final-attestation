package ru.agk13145.education.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "Auth request")
public class AuthRequestDto {

    @Schema(description = "Логин")
    private String username;

    @Schema(description = "Пароль")
    private String password;
}
