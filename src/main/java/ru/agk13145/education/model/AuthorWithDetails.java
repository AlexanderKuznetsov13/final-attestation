package ru.agk13145.education.model;

public class AuthorWithDetails extends Author {
    private Affiliation affiliation;

    public Affiliation getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(Affiliation affiliation) {
        this.affiliation = affiliation;
    }
}
