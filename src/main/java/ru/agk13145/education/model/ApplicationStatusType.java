package ru.agk13145.education.model;

import lombok.Data;

@Data
public class ApplicationStatusType {
    private Long id;
    private String name;
    private String description;

}
