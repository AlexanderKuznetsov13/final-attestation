package ru.agk13145.education.model;

import lombok.Data;

@Data
public class Topic {
    private Integer id;
    private String name;
}
