package ru.agk13145.education.model;

import lombok.Data;

@Data
public class Affiliation {
    private Long id;
    private String name;
}
