package ru.agk13145.education.model;

import lombok.Data;

@Data
public class User {
    private Integer id;
    private String name;
    private String surname;
    private String middleName;
    private String email;
    private String login;
    private String password;
    private Long appUserTypeId;
}
