package ru.agk13145.education.model.mapper.mapstruct;

import org.mapstruct.Mapper;
import ru.agk13145.education.dto.request. ApplicationStatusTypeRequestDto;
import ru.agk13145.education.dto.response. ApplicationStatusTypeResponseDto;
import ru.agk13145.education.entity. ApplicationStatusTypeEntity;
import ru.agk13145.education.model. ApplicationStatusType;

@Mapper(componentModel = "spring")
public interface ApplicationStatusTypeMapper {

     ApplicationStatusTypeResponseDto toResponseDto( ApplicationStatusType model);
     ApplicationStatusType toModel( ApplicationStatusTypeEntity model);
     ApplicationStatusType toModel( ApplicationStatusTypeRequestDto requestDto);
     ApplicationStatusTypeEntity toEntity( ApplicationStatusType model);

}
