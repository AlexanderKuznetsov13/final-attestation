package ru.agk13145.education.model.mapper.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import ru.agk13145.education.model.ApplicationStatusType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ApplicationStatusTypeRowMapper implements RowMapper<ApplicationStatusType> {

    @Override
    public ApplicationStatusType mapRow(ResultSet resultSet, int i) throws SQLException {
        ApplicationStatusType applicationStatusType = new ApplicationStatusType();
        applicationStatusType.setId(resultSet.getLong("ID"));
        applicationStatusType.setName(resultSet.getString("NAME"));
        applicationStatusType.setDescription(resultSet.getString("DESCRIPTION"));
        return applicationStatusType;
    }
}
