package ru.agk13145.education.model.mapper.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import ru.agk13145.education.model.Affiliation;


import java.sql.ResultSet;
import java.sql.SQLException;

public class AffiliationRowMapper implements RowMapper<Affiliation> {
    @Override
    public Affiliation mapRow(ResultSet resultSet, int i) throws SQLException {
        Affiliation affiliation = new Affiliation();
        affiliation.setId(resultSet.getLong("ID"));
        affiliation.setName(resultSet.getString("NAME"));
        return affiliation;
    }
}
