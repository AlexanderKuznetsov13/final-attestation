package ru.agk13145.education.model.mapper.rowmapper;

import ru.agk13145.education.model.Language;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LanguageRowMapper implements RowMapper<Language> {


    @Override
    public Language mapRow(ResultSet resultSet, int i) throws SQLException{
        Language language = new Language();
        language.setId(resultSet.getInt("ID"));
        language.setName(resultSet.getString("NAME"));
        return language;
    }

}
