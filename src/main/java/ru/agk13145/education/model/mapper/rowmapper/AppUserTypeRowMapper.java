package ru.agk13145.education.model.mapper.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import ru.agk13145.education.model.AppUserType;
import ru.agk13145.education.model.UserType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AppUserTypeRowMapper implements RowMapper<AppUserType> {

    @Override
    public AppUserType mapRow(ResultSet resultSet, int i) throws SQLException {
        AppUserType appUserType = new AppUserType();
        appUserType.setId(resultSet.getLong("ID"));
        appUserType.setName(resultSet.getString("NAME"));
        appUserType.setDescription(resultSet.getString("DESCRIPTION"));
        return appUserType;
    }
}
