package ru.agk13145.education.model.mapper.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.agk13145.education.dto.request.TopicRequestDto;
import ru.agk13145.education.dto.response.TopicResponseDto;
import ru.agk13145.education.entity.TopicEntity;
import ru.agk13145.education.model.Topic;

@Mapper(componentModel = "spring")
public interface TopicMapper {

    TopicResponseDto toResponseDto(Topic model);
    Topic toModel(TopicEntity model);
    Topic toModel(TopicRequestDto requestDto);
    TopicEntity toEntity(Topic model);

}
