package ru.agk13145.education.model.mapper.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import ru.agk13145.education.model.Config;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ConfigRowMapper implements RowMapper<Config> {

    @Override
    public Config mapRow(ResultSet resultSet, int i) throws SQLException {
        Config config = new Config();
        config.setId(resultSet.getInt("ID"));
        config.setName(resultSet.getString("NAME"));
        config.setDescription(resultSet.getString("DESCRIPTION"));
        return config;
    }
}
