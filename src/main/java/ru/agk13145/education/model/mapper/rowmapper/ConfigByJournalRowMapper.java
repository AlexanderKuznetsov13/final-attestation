package ru.agk13145.education.model.mapper.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import ru.agk13145.education.model.ConfigByJournal;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ConfigByJournalRowMapper implements RowMapper<ConfigByJournal> {

    @Override
    public ConfigByJournal mapRow(ResultSet resultSet, int i) throws SQLException {
        ConfigByJournal configByJournal = new ConfigByJournal();
        configByJournal.setId(resultSet.getInt("ID"));
        configByJournal.setConfigId(resultSet.getInt("CONFIG_ID"));
        configByJournal.setJournalId(resultSet.getInt("JOURNAL_ID"));
        configByJournal.setValue(resultSet.getString("VALUE"));
        return configByJournal;
    }
}
