package ru.agk13145.education.model.mapper.mapstruct;

import org.mapstruct.Mapper;
import ru.agk13145.education.dto.request.AffiliationRequestDto;
import ru.agk13145.education.dto.response.AffiliationResponseDto;
import ru.agk13145.education.entity.AffiliationEntity;
import ru.agk13145.education.model.Affiliation;

@Mapper(componentModel = "spring")
public interface AffiliationMapper {

    AffiliationResponseDto toResponseDto(Affiliation model);
    Affiliation toModel(AffiliationEntity model);
    Affiliation toModel(AffiliationRequestDto requestDto);
    AffiliationEntity toEntity(Affiliation model);

}
