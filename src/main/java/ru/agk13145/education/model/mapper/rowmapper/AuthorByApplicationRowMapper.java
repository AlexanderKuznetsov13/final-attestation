package ru.agk13145.education.model.mapper.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import ru.agk13145.education.model.AuthorByApplication;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorByApplicationRowMapper implements RowMapper<AuthorByApplication> {
    @Override
    public AuthorByApplication mapRow(ResultSet resultSet, int i) throws SQLException {
        AuthorByApplication authorByApplication = new AuthorByApplication();
        authorByApplication.setId(resultSet.getInt("ID"));
        authorByApplication.setApplicationId(resultSet.getInt("APPLICATION_ID"));
        authorByApplication.setAuthorByAffiliationId(resultSet.getInt("AUTHORS_BY_AFFILIATION_ID"));
        return authorByApplication;
    }
}
